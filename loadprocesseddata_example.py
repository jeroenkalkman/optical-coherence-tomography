     # -*- coding: utf-8 -*-
"""
Author = JdeWit
Date: 2023-03-02
email: J.deWit-1@tudelft.nl
email: J.Kalkman@tudelft.nl

This is a demo file to load processed data from a 2D or 3D acquisition
"""
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append(r'modules')
import ReadOCTfile
import DataProcessingOCT
import os
from PIL import Image

plt.close('all')
fdir = r".\data" #directory with the .oct file
dim = 2
fname = 'testimage_0001_Mode%dD.oct'%(dim) #.oct file filename
  
# load meta data
directory = ReadOCTfile.OCTfileOpen(os.path.join(fdir,fname))
header = ReadOCTfile.OCTreadHeader(directory)
Ascanav = int(header.Ocity.Acquisition.IntensityAveraging.AScans.string)
mode = header.Ocity.MetaInfo.AcquisitionMode.string
FOV = ReadOCTfile.OCTgetFOV(header)

#%% load and show the videoimage (RGB image from webcam)
videoimage = ReadOCTfile.OCTgetVideoImage(os.path.join(fdir,fname))
fig1 = plt.figure(1)
videoim = plt.imshow(videoimage)
videoim.axes.set_title('video image')
videoim.axes.set_xticks([])
videoim.axes.set_yticks([])

#%% load the processed image (cube in case of 3D dataset). This is the log compressed image with dimensions (x,y,z) for 3D and (z,x) for 2D
processedimage=ReadOCTfile.OCTgetProcessedImage(os.path.join(fdir,fname))
fig2 = plt.figure(2)
if mode == 'Mode3D':
    processed_im=plt.imshow(processedimage[:,64,:])
elif mode == 'Mode2D':
    processed_im=plt.imshow(processedimage)
processed_im.axes.set_title('processed image')
processed_im.axes.set_xticks([])
processed_im.axes.set_yticks([])

#%% load the preview image from the Thorlabs software (downsampled, not so quantitative so better use the processed image (above) for analysis)
preview_image=ReadOCTfile.OCTgetPreviewImage(os.path.join(fdir,fname))
fig3=plt.figure(3)
preview_im=plt.imshow(preview_image)
preview_im.axes.set_title('preview image')
preview_im.axes.set_xticks([])
preview_im.axes.set_yticks([])

#%% save processed images as uint16 tif files
saveastif = True
dBrange = 80 # the dynamic range in dB of the saved images. Everything below -80 dB will be put to the lowest value
fdir_save = r'.\tifsavedirectory' # directory where the tif files will be saved
if saveastif:
    if not os.path.exists(fdir_save):
       os.mkdir(fdir_save)
    processedimage_uint16 = processedimage-processedimage.max()+dBrange;
    processedimage_uint16[processedimage_uint16<0]=0;
    processedimage_uint16 = np.uint16(processedimage_uint16/processedimage_uint16.max()*(2**16-1));
    if mode == 'Mode3D':
        [Ny,Nx,Nz] = processedimage_uint16.shape
    elif mode == 'Mode2D':
        [Nz,Nx] = processedimage_uint16.shape
        Ny = 1
    # write metadata, such as sampling density and field of view in .txt file for easys accessibility
    f = open(os.path.join(fdir_save,'metadata.txt'), 'w')
    f.write('sampling distances for file %s \n'%(fname))
    f.write('Lz = %f mm \n'%(FOV[0]*1e3))
    f.write('dz = %f um \n'%(FOV[0]/Nz*1e6))
    f.write('Lx = %f mm \n'%(FOV[1]*1e3))
    f.write('dx = %f um \n'%(FOV[1]/Nx*1e6))
    if mode == 'Mode3D':
        f.write('Ly = %f mm \n'%(FOV[2]*1e3))
        f.write('dy = %f um \n'%(FOV[2]/Ny*1e6))
    f.write('for physical distance, dz needs to be divided through the refractive index; dx and dy remain the same')
    f.close()
    
    if mode == 'Mode3D':
        tifdir = os.path.join(fdir_save,'tifs')
        if not os.path.exists(tifdir):
            os.mkdir(tifdir)
        for i in range(processedimage_uint16.shape[2]):
            im_tif = Image.fromarray(processedimage_uint16[:,:,i])
            im_tif.save(os.path.join(tifdir,r'zframe%d.tif'%(i)))
    elif mode == 'Mode2D':
        im_tif = Image.fromarray(processedimage_uint16)
        im_tif.save(os.path.join(fdir_save,r'processed_image.tif'))
    
    vid_im = Image.fromarray(videoimage)
    vid_im.save(os.path.join(fdir_save,r'reference_image.tif'))
    