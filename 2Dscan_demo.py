"""
2Dscan_demo

Author = JdeWit
Date: 2019-08-15
email: J.deWit-1@tudelft.nl

This is a demo file to load and process 1D or 2D OCT data. 
1D data will still be plotted as a 2D dataset, the horizontal axis being the Ascan number
"""
# load modules
import numpy as np
from time import time
import sys
sys.path.append('modules')
import ReadOCTfile
import DataProcessingOCT

# load dechirp to be used in the calculations
dechirp=np.fromfile('modules\Chirp.data',np.float32)
# set data_folder and file name
data_folder="data\\"
file_name="Default_0001_Mode2D.oct"

#%%
t0=time()
# load the data from the file specified above
header,rawdata,spectrum,FOV=ReadOCTfile.OCTgetDataCombined(data_folder+file_name,spectrumindex=0)   
Ascanav=int(header.Ocity.Acquisition.IntensityAveraging.AScans.string)
# process the data into a Bscan image
image=DataProcessingOCT.calc_Bscan(rawdata,spectrum,dechirp,Ascanav,apodization='hanning',filters='none',objective='LK4')
t1=time()
print('It took ',t1-t0,' s to load and process file ',file_name)
    
#%% plot the obtained image
fig1=DataProcessingOCT.plot_Bscan_image(image,dBlevel=80,FOV=FOV,title='image (dB) file: '+file_name,plot_no=0)

  
    
