"""
2Dscan_demo

Author = JdeWit
Date: 2019-08-15
email: J.deWit-1@tudelft.nl

This is a demo file to load and process 3D OCT data. 
The data will be returned as a 3D matrix which the user can process further as he wishes.
"""

#import modules
import numpy as np
from time import time
import ReadOCTfile
import DataProcessingOCT

#%%
# load dechirp file and set data name and path
dechirp=np.fromfile('modules\Chirp.data',np.float32)
data_folder="data//"
data_name="testdata_0002_Mode3D.oct"

# get pointer to the directory inside the .oct file and extract relevant information from header
directory=ReadOCTfile.OCTfileOpen(data_folder+data_name)
header=ReadOCTfile.OCTreadHeader(directory)
Ny=int(header.Image.SizeY.string)
Nx=int(header.Image.SizeX.string)#+1
Nz=int(header.Image.SizeZ.string)
Ascanav=int(header.Ocity.Acquisition.IntensityAveraging.AScans.string)
FOV=ReadOCTfile.OCTgetFOV(header)

# make an empty file to store the 3D data in
Image3D=np.empty([Nz,Nx,Ny], np.complex64)
# calculate the image per Bscan frame and store in the matrix Image3D
for i in range(Ny):
    t0=time()
    rawdata,spectrum=ReadOCTfile.OCTgetRawData(directory,header,spectrumindex=i)
    Bscan=DataProcessingOCT.calc_Bscan(rawdata,spectrum,dechirp,Ascanav)
    Image3D[:,:,i]=Bscan
    t1=time()
    print(t1-t0,' s have passed for line ',i)

#%% save the absolute value of the calculated 3D data
folder='data\\'
np.save(folder+'processed_'+data_name[0:-4],np.abs(Image3D))

  
    
